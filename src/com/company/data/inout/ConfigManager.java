package com.company.data.inout;

/**
 * Interfejs dla klas implementujących własne metody zapisywania i odczytywania konfiguracji.
 */
interface ConfigManager {
    /**
     * Odczytuje konfigurację.
     *
     * @return obiekt Config przechowujący odczytaną konfigurację
     * @throws ConfigManagerException jeżeli konfiguracja nie przechodzi walidacji lub odczytanie nie powiodło się
     */
    Config getConfig() throws ConfigManagerException;

    /**
     * Zapisuje konfigurację.
     *
     * @param config konfiguracja do zapisania
     * @throws ConfigManagerException jeżeli zapis się nie powiódł
     */
    void saveConfig(Config config) throws ConfigManagerException;
}
