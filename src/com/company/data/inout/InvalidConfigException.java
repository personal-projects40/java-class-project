package com.company.data.inout;

/**
 * Wyrzucony, gdy konfiguracja jest nieprawidłowa.
 */
public class InvalidConfigException extends Exception {

    /**
     * Konstruuje {@link InvalidConfigException} ze szczegółowym komunikatem.
     *
     * @param message szczegółowy komunikat
     */
    public InvalidConfigException(String message) {
        super(message);
    }
}
